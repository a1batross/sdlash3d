#ifndef KEYWRAPPER_H
#define KEYWRAPPER_H

#include "recdefs.h"
#include "common.h"
#include "SDL2/SDL.h"

int SDLEventFilter(void *userdata, SDL_Event* event);
void SDLKeyEvent(SDL_KeyboardEvent key);
void SDLMouseEvent(SDL_MouseButtonEvent button);
void SDLWheelEvent(SDL_MouseWheelEvent wheel);

#endif // KEYWRAPPER_H
