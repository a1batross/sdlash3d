#include "events.h"
#include "keydefs.h"
#include "input.h"

int SDLEventFilter( void* userdata, SDL_Event* event)
{
    switch ( event->type )
    {
        case SDL_MOUSEMOTION:
            IN_MouseEvent(event->motion, event->motion.state);
            break;
        case SDL_QUIT:
            Host_Shutdown();
            break;

        case SDL_KEYDOWN:
        case SDL_KEYUP:
            SDLKeyEvent(event->key);
            break;

        case SDL_MOUSEWHEEL:
            SDLWheelEvent(event->wheel);
            break;

        case SDL_MOUSEBUTTONUP:
        case SDL_MOUSEBUTTONDOWN:
            SDLMouseEvent(event->button);
            break;
        case SDL_WINDOWEVENT:
            if( host.state == HOST_SHUTDOWN )
                break; // no need to activate
            if( host.state != HOST_RESTART )
            {
                switch( event->window.type )
                {
                case SDL_WINDOWEVENT_MINIMIZED:
                    host.state = HOST_SLEEP;
                    break;
                case SDL_WINDOWEVENT_FOCUS_LOST:
                    host.state = HOST_NOFOCUS;
                    IN_DeactivateMouse();
                    break;
                default:
                    host.state = HOST_FRAME;
                    IN_ActivateMouse(true);
                }
            }
    }
    return 0;
}
void SDLKeyEvent(SDL_KeyboardEvent key)
{
    // TODO: improve that.
    int keynum = key.keysym.sym;
    int down = key.type == SDL_KEYDOWN ? 1 : 0;
    if(key.repeat) return;
    switch(key.keysym.sym)
    {
    case SDLK_BACKSPACE:
        keynum =  K_BACKSPACE;
        break;
    case SDL_SCANCODE_UP:
        keynum = K_UPARROW;
        break;
    case SDL_SCANCODE_DOWN:
        keynum = K_DOWNARROW;
        break;
    case SDL_SCANCODE_LEFT:
        keynum = K_LEFTARROW;
        break;
    case SDL_SCANCODE_RIGHT:
        keynum = K_RIGHTARROW;
        break;
    case SDLK_LALT:
    case SDLK_RALT:
        keynum = K_ALT;
        break;
    case SDLK_RSHIFT:
    case SDLK_LSHIFT:
        keynum = K_SHIFT;
        break;
    case SDLK_LCTRL:
    case SDLK_RCTRL:
        keynum = K_CTRL;
        break;
    case SDLK_INSERT:
        keynum = K_INS;
        break;
    case SDLK_DELETE:
        keynum = K_DEL;
        break;
    case SDLK_PAGEUP:
        keynum = K_PGUP;
        break;
    case SDLK_PAGEDOWN:
        keynum = K_PGDN;
        break;
    case SDLK_HOME:
        keynum = K_HOME;
        break;
    case SDLK_END:
        keynum = K_END;
        break;
    }

    if((key.keysym.sym >= SDLK_F1) && (key.keysym.sym <= SDLK_F12))
    {
        keynum = key.keysym.scancode + 77;
    }
    if((key.keysym.sym >= SDLK_KP_1) && (key.keysym.sym <= SDLK_KP_0))
    {
        keynum = key.keysym.scancode - 41;
    }
    //printf("Pressed key. Code: %i\n", keynum);
    Key_Event(keynum, down);
}

void SDLMouseEvent(SDL_MouseButtonEvent button)
{
    int down = button.type == SDL_MOUSEBUTTONDOWN ? 1 : 0;
    Key_Event(240 + button.button, down);
}

void SDLWheelEvent(SDL_MouseWheelEvent wheel)
{
    int updown = wheel.x < 0 ? K_MWHEELDOWN : K_MWHEELDOWN;
    Key_Event(updown, true);
    Key_Event(updown, false);
}
