/*
vox.h - sentences vox private header
Copyright (C) 2010 Uncle Mike

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/

#ifndef VOX_H
#define VOX_H

#define CVOXWORDMAX			32
#define CVOXSENTENCEMAX		24
#define CVOXZEROSCANMAX		255	// scan up to this many samples for next zero crossing
#define MAX_SENTENCES		2048
#define SENTENCE_INDEX		-99999	// unique sentence index

typedef struct voxword_s
{
	int	volume;		// increase percent, ie: 125 = 125% increase
	int	pitch;		// pitch shift up percent
	int	start;		// offset start of wave percent
	int	end;		// offset end of wave percent
	int	cbtrim;		// end of wave after being trimmed to 'end'
	int	fKeepCached;	// 1 if this word was already in cache before sentence referenced it
	int	samplefrac;	// if pitch shifting, this is position into wav * 256
	int	timecompress;	// % of wave to skip during playback (causes no pitch shift)
	sfx_t	*sfx;		// name and cache pointer
} voxword_t;

typedef struct channel_s
{
    char		name[16];		// keept sentence name
    sfx_t		*sfx;		// sfx number

    int		leftvol;		// 0-255 left volume
    int		rightvol;		// 0-255 right volume

    int		entnum;		// entity soundsource
    int		entchannel;	// sound channel (CHAN_STREAM, CHAN_VOICE, etc.)
    vec3_t		origin;		// only use if fixed_origin is set
    float		dist_mult;	// distance multiplier (attenuation/clipK)
    int		master_vol;	// 0-255 master volume
    qboolean		isSentence;	// bit who indicated sentence
    int		basePitch;	// base pitch percent (100% is normal pitch playback)
    float		pitch;		// real-time pitch after any modulation or shift by dynamic data
    qboolean		use_loop;		// don't loop default and local sounds
    qboolean		staticsound;	// use origin instead of fetching entnum's origin
    qboolean		localsound;	// it's a local menu sound (not looped, not paused)
    mixer_t		pMixer;

    // sound culling
    qboolean		bfirstpass;	// true if this is first time sound is spatialized
    float		ob_gain;		// gain drop if sound source obscured from listener
    float		ob_gain_target;	// target gain while crossfading between ob_gain & ob_gain_target
    float		ob_gain_inc;	// crossfade increment
    qboolean		bTraced;		// true if channel was already checked this frame for obscuring
    float		radius;		// radius of this sound effect

    // sentence mixer
    int		wordIndex;
    mixer_t		*currentWord;	// NULL if sentence is finished
    voxword_t		words[CVOXWORDMAX];
} channel_t;



typedef struct
{
	char	*pName;
	float	length;
} sentence_t;

void VOX_LoadWord( struct channel_s *pchan );
void VOX_FreeWord( struct channel_s *pchan );

#endif
