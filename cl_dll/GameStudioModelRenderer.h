//========= Copyright � 1996-2002, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#if !defined( GAMESTUDIOMODELRENDERER_H )
#define GAMESTUDIOMODELRENDERER_H
#if defined( _WIN32 )
#pragma once
#endif

/*
====================
CGameStudioModelRenderer

====================
*/
class CGameStudioModelRenderer : public CStudioModelRenderer
{
public:
	CGameStudioModelRenderer( void );
    void StudioSetupBones( void );
    void StudioEstimateGait( entity_state_t *pplayer );
    void StudioProcessGait( entity_state_t *pplayer );
    void SavePlayerState( entity_state_t *pplayer );
    void SetupClientAnimation( entity_state_t *pplayer );
    void RestorePlayerState( entity_state_t *pplayer );
    int StudioDrawPlayer(int flags, entity_state_t *pplayer);
    int _StudioDrawPlayer(int flags, entity_state_t *pplayer);
    void StudioFxTransform( cl_entity_t *ent, float transform[3][4] );
};

#endif // GAMESTUDIOMODELRENDERER_H
