/*
game.cpp -- executable to run Xash Engine
Copyright (C) 2011 Uncle Mike

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/

#ifdef _WIN32
#include <windows.h>
#else
#include "recdefs.h"
// Compiler find a error in stdlib.h
#undef __USE_ISOC11
#include <stdlib.h>
#include <sys/types.h>
#include <dlfcn.h>
#include <cstdio>
#include <common.h>

//SDL2
#include <SDL2/SDL.h>
#endif


#define GAME_PATH	"valve"	// default dir to start from

#ifdef DYNAMIC
typedef void (*pfnChangeGame)( const char *progname );
typedef int (*pfnInit)( char *cmd_Line, const char *progname, int bChangeGame, pfnChangeGame func );
typedef void (*pfnShutdown)( void );

pfnInit Host_Main;
pfnShutdown Host_Shutdown = NULL;
#endif
char szGameDir[128]; // safe place to keep gamedir
char *cmdLine;
void *hEngine;

void Launch_Error( const char *errorstring )
{
#ifdef _WIN32
	MessageBox( NULL, errorstring, "Xash Error", MB_OK|MB_SETFOREGROUND|MB_ICONSTOP );
#else
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Xash Error", errorstring, NULL);
#endif
	exit( 1 );
}

void Sys_LoadEngine( void )
{
#ifdef DYNAMIC
#ifdef _DEDICATED
#ifdef _WIN32
	if(( hEngine = LoadLibrary( "dedicated.dll" )) == NULL )
	{
		Sys_Error( "Unable to load the dedicated.dll" );
	}

	if(( Host_Main = (pfnInit)GetProcAddress( hEngine, "Host_Main" )) == NULL )
	{
		Sys_Error( "xash.dll missed 'Host_Main' export" );
	}

	// this is non-fatal for us but change game will not working
	Host_Shutdown = (pfnShutdown)GetProcAddress( hEngine, "Host_Shutdown" );
#else
	if(( hEngine = dlopen( "dedicated.so", RTLD_NOW )) == NULL )
	{
		Sys_Error( "Unable to load the dedicated.so" );
	}

	if(( Host_Main = (pfnInit)dlsym( hEngine, "Host_Main" )) == NULL )
	{
		Sys_Error( "xash.dll missed 'Host_Main' export" );
	}

	// this is non-fatal for us but change game will not working
	Host_Shutdown = (pfnShutdown)dlsym( hEngine, "Host_Shutdown" );
#endif
#else
#ifdef _WIN32
	if(( hEngine = LoadLibrary( "xash.dll" )) == NULL )
	{
		Sys_Error( "Unable to load the xash.dll" );
	}

	if(( Host_Main = (pfnInit)GetProcAddress( hEngine, "Host_Main" )) == NULL )
	{
		Sys_Error( "xash.dll missed 'Host_Main' export" );
	}

	// this is non-fatal for us but change game will not working
	Host_Shutdown = (pfnShutdown)GetProcAddress( hEngine, "Host_Shutdown" );
#else
    if(( hEngine = dlopen( "xash.so", RTLD_LAZY )) == NULL )
	{
		Sys_Error( "Unable to load the xash.so" );
        puts( dlerror() );
	}

	if(( Host_Main = (pfnInit)dlsym( hEngine, "Host_Main" )) == NULL )
	{
		Sys_Error( "xash.dll missed 'Host_Main' export" );
        puts( dlerror() );
	}

	// this is non-fatal for us but change game will not working
	Host_Shutdown = (pfnShutdown)dlsym( hEngine, "Host_Shutdown" );
#endif
#endif
#endif
}

void Sys_UnloadEngine( void )
{
#ifdef DYNAMIC
	if( Host_Shutdown ) Host_Shutdown( );

#ifdef _WIN32
	if( hEngine ) FreeLibrary( hEngine );
#else
	if( hEngine ) dlclose( hEngine );
#endif
#endif
}

void Sys_ChangeGame( const char *progname )
{
    if( !progname || !progname[0] )  Launch_Error( "Sys_ChangeGame: NULL gamedir" );
    if( Host_Shutdown == NULL ) Launch_Error( "Sys_ChangeGame: missed 'Host_Shutdown' export\n" );
	strncpy( szGameDir, progname, sizeof( szGameDir ) - 1 );

	Sys_UnloadEngine ();
	Sys_LoadEngine ();

    Host_Main( cmdLine, szGameDir, TRUE, ( Host_Shutdown != NULL ) ? Sys_ChangeGame : NULL );
}

#ifdef _WIN32
int APIENTRY WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
#else
int main(int argc, char *argv[])
#endif
{
    if(SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0)
    {
        fprintf(stderr, "Unable to initialize SDL: %s", SDL_GetError());
        return 1;
    }


    int argLen = 1; // including \0 character
    for(int i = 1; i < argc; ++i)
    {
        argLen += strlen( argv[i] );
    }

    cmdLine = (char *)calloc(0, argLen * sizeof(char));
    for(int i = 1; i < argc; ++i)
    {
        strcat(cmdLine, argv[i]);
        strcat(cmdLine, " ");
    }
	Sys_LoadEngine();

    return Host_Main( cmdLine, GAME_PATH, FALSE, ( Host_Shutdown != NULL ) ? Sys_ChangeGame : NULL );
}
